﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weight
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            TxtHeight.Text = "160";
            RdbMan.Checked = true;
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            double height = Convert.ToDouble(TxtHeight.Text);
            double weight;
            if (RdbMan.Checked == true)
            {
                weight = (height - 80) * 0.7;
            }
            else
            {
                weight = (height - 70) * 0.6;
            }
            LblMsg.Text = "Standard weight : " + weight.ToString("f2") + " kg";
        }
    }
}
