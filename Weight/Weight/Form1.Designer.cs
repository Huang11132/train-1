﻿namespace Weight
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnOK = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtHeight = new System.Windows.Forms.TextBox();
            this.RdbMan = new System.Windows.Forms.RadioButton();
            this.RdbWoman = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LblMsg = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnOK
            // 
            this.BtnOK.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.BtnOK.Location = new System.Drawing.Point(554, 159);
            this.BtnOK.Margin = new System.Windows.Forms.Padding(4);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(130, 40);
            this.BtnOK.TabIndex = 0;
            this.BtnOK.Text = "OK";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.label1.Location = new System.Drawing.Point(74, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(350, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Please enter the height (cm) : ";
            // 
            // TxtHeight
            // 
            this.TxtHeight.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.TxtHeight.Location = new System.Drawing.Point(432, 74);
            this.TxtHeight.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHeight.Name = "TxtHeight";
            this.TxtHeight.Size = new System.Drawing.Size(252, 39);
            this.TxtHeight.TabIndex = 2;
            // 
            // RdbMan
            // 
            this.RdbMan.AutoSize = true;
            this.RdbMan.Location = new System.Drawing.Point(35, 40);
            this.RdbMan.Margin = new System.Windows.Forms.Padding(4);
            this.RdbMan.Name = "RdbMan";
            this.RdbMan.Size = new System.Drawing.Size(94, 34);
            this.RdbMan.TabIndex = 3;
            this.RdbMan.TabStop = true;
            this.RdbMan.Text = "Male";
            this.RdbMan.UseVisualStyleBackColor = true;
            // 
            // RdbWoman
            // 
            this.RdbWoman.AutoSize = true;
            this.RdbWoman.Location = new System.Drawing.Point(149, 40);
            this.RdbWoman.Margin = new System.Windows.Forms.Padding(4);
            this.RdbWoman.Name = "RdbWoman";
            this.RdbWoman.Size = new System.Drawing.Size(120, 34);
            this.RdbWoman.TabIndex = 4;
            this.RdbWoman.TabStop = true;
            this.RdbWoman.Text = "Female";
            this.RdbWoman.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RdbMan);
            this.groupBox1.Controls.Add(this.RdbWoman);
            this.groupBox1.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.groupBox1.Location = new System.Drawing.Point(79, 159);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(293, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gender";
            // 
            // LblMsg
            // 
            this.LblMsg.AutoSize = true;
            this.LblMsg.Location = new System.Drawing.Point(399, 229);
            this.LblMsg.Name = "LblMsg";
            this.LblMsg.Size = new System.Drawing.Size(218, 30);
            this.LblMsg.TabIndex = 6;
            this.LblMsg.Text = "Standard weight : ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 324);
            this.Controls.Add(this.LblMsg);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TxtHeight);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnOK);
            this.Font = new System.Drawing.Font("微軟正黑體", 12F);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Form1";
            this.Text = "Standard weight calculator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtHeight;
        private System.Windows.Forms.RadioButton RdbMan;
        private System.Windows.Forms.RadioButton RdbWoman;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label LblMsg;
    }
}

