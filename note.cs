﻿
Console.ReadKey();           //停留直到按下任意按鍵


Console.WriteLine($"quotient: {d}");     //組合字串
Console.WriteLine(aFriend);  //顯示字串
aFriend.Length;              //回傳字串長度

greeting.TrimStart();        //去除前段空格
greeting.TrimEnd();          //去除後段空格
greeting.Trim();             //去除前後空格

sayHello.Replace("Hello", "Greetings");     //將字串中的Hello換成Greetings
sayHello.ToUpper();          //全部換成大寫
sayHello.ToLower();          //全部換成小寫

songLyrics.Contains("goodbye")     //判斷字串是否包含特定字串
songLyrics.StartsWith("You")       //判斷字串開頭是否包含特定字串
songLyrics.EndsWith("You")         //判斷字串結尾是否包含特定字串


int.MaxValue;        //最大整數
int.MinValue;        //最小整數
double.MaxValue;     //最大浮點數 (範圍最大)
double.MinValue;     //最小浮點數
decimal.MinValue;    //最大decimal (範圍中，但小數精度較double高)
decimal.MaxValue;    //最大decimal
decimal c = 1.0M;
Math.PI              // pi
Math.E               // e
Math.Pow(a,b)        // a^b
Random rnd1 = new Random();     //回傳0~1之間的亂數
MessageBox.Show($"number is {rnd1.NextDouble()}");

int n = int.Parse("2017");           //將字串2017轉為整數
double d = double.Parse("3.14");     //將字串3.14轉為浮點數
string str1 = d.ToString();          //將3.14轉為字串

//if迴圈 && ||
if ((a + b + c > 10) && (a == b))      
    {
    Console.WriteLine("The answer is greater than 10");
    Console.WriteLine("And the first number is equal to the second");
    }
else
    {
    Console.WriteLine("The answer is not greater than 10");
    Console.WriteLine("Or the first number is not equal to the second");
    }

//while 迴圈
int counter = 0;
while (counter <= 10)
      {
      Console.WriteLine($"Hello World! The counter is {counter}");
      counter++;
      }

//do...while 迴圈
int counter = 0;
do
{
  Console.WriteLine($"Hello World! The counter is {counter}");
  counter++;
} while (counter< 10);

//for 迴圈
for(int counter = 0; counter< 10; counter++)
{
  Console.WriteLine($"Hello World! The counter is {counter}");
}


var names = new List<string> { "<name>", "Ana", "Felipe" };     //字串清單
List<string> lines = new List<string> { "<name>", "Ana", "Felipe" };
string[] lines = { "<name>", "Ana", "Felipe" };
names.Add("Maria");      //增加
names.Remove("Ana");     //減少
name.ToUpper();          //全部換大寫
names[0];                //第一個元素
names.Count;             //長度
var index = names.IndexOf("Felipe");     //搜尋特定元素
names.Sort();            //排列

var fibonacciNumbers = new List<int> { 1, 1 };                    //數字清單
var previous = fibonacciNumbers[fibonacciNumbers.Count - 1];      //第end個元素
var previous2 = fibonacciNumbers[fibonacciNumbers.Count - 2];     //第end-1個元素
fibonacciNumbers.Add(previous + previous2);                       //新增元素

// Label 沒有 tab \t
// TextBox 換行需使用 \r\n