﻿using System;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;

class Program
{
    static void Main(string[] args)
    {
        // Evaluate a special function
        Console.WriteLine(SpecialFunctions.Erf(0.5) + "\n");

        // Solve a random linear equation system with 5 unknowns
        Matrix<double> m = Matrix<double>.Build.Random(5, 5);
        Vector<double> v = Vector<double>.Build.Random(5);
        Vector<double> y = m.Solve(v);
        Console.WriteLine(y + "\n");

        // Inverse matrix
        Matrix<double> m2 = m.Inverse();
        Console.WriteLine(m + "\n");
        Console.WriteLine(m2 + "\n");

        Console.ReadKey();
    }
}