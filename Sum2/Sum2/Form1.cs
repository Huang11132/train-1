﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sum2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int CountSum(int n, int m)
        {
            int total = 0;
            for (int i = n; i <= m; i++)
            {
                total += i;
            }
            return total;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TxtM.TabIndex = 0;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int m = Convert.ToInt32(TxtM.Text);
            int n = Convert.ToInt32(TxtN.Text);
            int tot = 0;
            tot = CountSum(m, n);
            MessageBox.Show(m.ToString() + "加到" + m.ToString() + "的總和為 : " + tot.ToString());
            TxtM.Focus();
        }
    }
}
