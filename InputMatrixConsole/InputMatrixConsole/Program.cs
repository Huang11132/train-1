﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i, j, row, col;
            Console.Write("Input row in the matrix :\n");
            row = Convert.ToInt32(Console.ReadLine());
            Console.Write("\nInput column in the matrix :\n");
            col = Convert.ToInt32(Console.ReadLine());
            int[,] arr1 = new int[row, col];

            /* Stored values into the array*/
            Console.Write("\nInput elements in the matrix :\n");
            for (i = 0; i < row; i++)
            {
                for (j = 0; j < row; j++)
                {
                    Console.Write("element - [{0},{1}] : ", i + 1, j + 1);
                    arr1[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }

            Console.Write("\nThe matrix is : \n");
            for (i = 0; i < row; i++)
            {
                Console.Write("\n");
                for (j = 0; j < row; j++)
                    Console.Write("{0}\t", arr1[i, j]);
            }
            Console.Write("\n\n");
            Console.ReadKey();
        }
    }
}
