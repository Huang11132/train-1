﻿namespace Sum1
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.LblMN = new System.Windows.Forms.Label();
            this.TxtN = new System.Windows.Forms.TextBox();
            this.TxtM = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.LblM = new System.Windows.Forms.Label();
            this.LblN = new System.Windows.Forms.Label();
            this.LblAns = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LblMN
            // 
            this.LblMN.AutoSize = true;
            this.LblMN.Font = new System.Drawing.Font("新細明體", 12F);
            this.LblMN.Location = new System.Drawing.Point(68, 264);
            this.LblMN.Name = "LblMN";
            this.LblMN.Size = new System.Drawing.Size(89, 24);
            this.LblMN.TabIndex = 0;
            this.LblMN.Text = "m + n = ";
            // 
            // TxtN
            // 
            this.TxtN.Font = new System.Drawing.Font("新細明體", 12F);
            this.TxtN.Location = new System.Drawing.Point(197, 159);
            this.TxtN.Name = "TxtN";
            this.TxtN.Size = new System.Drawing.Size(100, 36);
            this.TxtN.TabIndex = 1;
            // 
            // TxtM
            // 
            this.TxtM.Font = new System.Drawing.Font("新細明體", 12F);
            this.TxtM.Location = new System.Drawing.Point(197, 75);
            this.TxtM.Name = "TxtM";
            this.TxtM.Size = new System.Drawing.Size(100, 36);
            this.TxtM.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("新細明體", 12F);
            this.button1.Location = new System.Drawing.Point(344, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 48);
            this.button1.TabIndex = 3;
            this.button1.Text = "計算";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // LblM
            // 
            this.LblM.AutoSize = true;
            this.LblM.Font = new System.Drawing.Font("新細明體", 12F);
            this.LblM.Location = new System.Drawing.Point(68, 84);
            this.LblM.Name = "LblM";
            this.LblM.Size = new System.Drawing.Size(53, 24);
            this.LblM.TabIndex = 4;
            this.LblM.Text = "m = ";
            // 
            // LblN
            // 
            this.LblN.AutoSize = true;
            this.LblN.Font = new System.Drawing.Font("新細明體", 12F);
            this.LblN.Location = new System.Drawing.Point(68, 168);
            this.LblN.Name = "LblN";
            this.LblN.Size = new System.Drawing.Size(46, 24);
            this.LblN.TabIndex = 5;
            this.LblN.Text = "n = ";
            // 
            // LblAns
            // 
            this.LblAns.Font = new System.Drawing.Font("新細明體", 12F);
            this.LblAns.Location = new System.Drawing.Point(193, 264);
            this.LblAns.Name = "LblAns";
            this.LblAns.Size = new System.Drawing.Size(100, 36);
            this.LblAns.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 402);
            this.Controls.Add(this.LblAns);
            this.Controls.Add(this.LblN);
            this.Controls.Add(this.LblM);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.TxtM);
            this.Controls.Add(this.TxtN);
            this.Controls.Add(this.LblMN);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblMN;
        private System.Windows.Forms.TextBox TxtN;
        private System.Windows.Forms.TextBox TxtM;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label LblM;
        private System.Windows.Forms.Label LblN;
        private System.Windows.Forms.Label LblAns;
    }
}

