﻿namespace Area
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.w = new System.Windows.Forms.Label();
            this.h = new System.Windows.Forms.Label();
            this.area = new System.Windows.Forms.Label();
            this.TxtH = new System.Windows.Forms.TextBox();
            this.TxtArea = new System.Windows.Forms.TextBox();
            this.BtnOK = new System.Windows.Forms.Button();
            this.TxtW = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // w
            // 
            this.w.AutoSize = true;
            this.w.Font = new System.Drawing.Font("新細明體", 11F);
            this.w.Location = new System.Drawing.Point(81, 73);
            this.w.Name = "w";
            this.w.Size = new System.Drawing.Size(112, 22);
            this.w.TabIndex = 0;
            this.w.Text = "寬度(公尺)";
            // 
            // h
            // 
            this.h.AutoSize = true;
            this.h.Font = new System.Drawing.Font("新細明體", 11F);
            this.h.Location = new System.Drawing.Point(81, 161);
            this.h.Name = "h";
            this.h.Size = new System.Drawing.Size(112, 22);
            this.h.TabIndex = 1;
            this.h.Text = "長度(公尺)";
            // 
            // area
            // 
            this.area.AutoSize = true;
            this.area.Font = new System.Drawing.Font("新細明體", 11F);
            this.area.Location = new System.Drawing.Point(81, 244);
            this.area.Name = "area";
            this.area.Size = new System.Drawing.Size(54, 22);
            this.area.TabIndex = 2;
            this.area.Text = "面積";
            // 
            // TxtH
            // 
            this.TxtH.Font = new System.Drawing.Font("新細明體", 11F);
            this.TxtH.Location = new System.Drawing.Point(240, 158);
            this.TxtH.Name = "TxtH";
            this.TxtH.Size = new System.Drawing.Size(150, 34);
            this.TxtH.TabIndex = 4;
            // 
            // TxtArea
            // 
            this.TxtArea.Font = new System.Drawing.Font("新細明體", 11F);
            this.TxtArea.Location = new System.Drawing.Point(240, 241);
            this.TxtArea.Name = "TxtArea";
            this.TxtArea.Size = new System.Drawing.Size(150, 34);
            this.TxtArea.TabIndex = 5;
            // 
            // BtnOK
            // 
            this.BtnOK.Font = new System.Drawing.Font("新細明體", 11F);
            this.BtnOK.Location = new System.Drawing.Point(432, 152);
            this.BtnOK.Name = "BtnOK";
            this.BtnOK.Size = new System.Drawing.Size(117, 40);
            this.BtnOK.TabIndex = 6;
            this.BtnOK.Text = "計算";
            this.BtnOK.UseVisualStyleBackColor = true;
            this.BtnOK.Click += new System.EventHandler(this.BtnOK_Click);
            // 
            // TxtW
            // 
            this.TxtW.Font = new System.Drawing.Font("新細明體", 11F);
            this.TxtW.Location = new System.Drawing.Point(240, 70);
            this.TxtW.Name = "TxtW";
            this.TxtW.Size = new System.Drawing.Size(150, 34);
            this.TxtW.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 357);
            this.Controls.Add(this.TxtW);
            this.Controls.Add(this.BtnOK);
            this.Controls.Add(this.TxtArea);
            this.Controls.Add(this.TxtH);
            this.Controls.Add(this.area);
            this.Controls.Add(this.h);
            this.Controls.Add(this.w);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label w;
        private System.Windows.Forms.Label h;
        private System.Windows.Forms.Label area;
        private System.Windows.Forms.TextBox TxtH;
        private System.Windows.Forms.TextBox TxtArea;
        private System.Windows.Forms.Button BtnOK;
        private System.Windows.Forms.TextBox TxtW;
    }
}

